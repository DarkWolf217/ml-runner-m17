﻿using UnityEngine;

// script del movimiento y comportamiento del enemigo
public class enemyMovement : MonoBehaviour
{
    Rigidbody2D rb2; //para referirme al rigidbody 
    public float vel; //velocidad del enemigo

    // Start is called before the first frame update
    void Start()
    {
        rb2 = GetComponent<Rigidbody2D>(); //asigno rigidbody a rb2
        rb2.velocity = new Vector2(-3f, 0f); //asigno la velocidad al enemigo

        if (this.gameObject.transform.localPosition.y >= 0f) //según su spawn su gravedad comenzará siendo positiva o negativa
        {
            rb2.gravityScale = -(rb2.gravityScale);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.transform.localPosition.x < -23f)
        {
            GameObject.Destroy(this.gameObject);
        }
        move();
    }

    // movimiento del enemigo
    private void move()
    {
        transform.Translate(Vector3.left * vel * Time.deltaTime);        
    }

    // colisiones
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "Floor") //si toca el suelo se invierte la gravedad
        {
            rb2.gravityScale = -(rb2.gravityScale);
        }
        if (other.transform.tag == "Enemy") //si toca un enemigo, lo atraviesa
        {
            Physics2D.IgnoreCollision(this.gameObject.GetComponent<Collider2D>(), other.gameObject.GetComponent<Collider2D>()); //ignora la colisión
            
        }
    }
}
