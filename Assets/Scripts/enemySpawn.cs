﻿using UnityEngine;

// script del spawn de enemigos
public class enemySpawn : MonoBehaviour
{
    public GameObject enemy; //referencia al enemigo
    public float spawnVel; //velocidad de spawn
    public GameObject score; //referencia a la puntuación 
    public GameObject player; //referencia al jugador

    // Start is called before the first frame update
    void Start()
    {
        Invoke("spawner", 1f); //cada 40 segundos aparece un enemigo extra
    }

    // llama a spawn cada x tiempo (depende de la velocidad que le asignes)
    void spawner()
    {
        InvokeRepeating("spawn", 2f, spawnVel);
    }

    // spawn
    void spawn()
    {        
        if (true) //si el jugador sigue vivo spawnea
        {
            GameObject newEnemy = Instantiate(enemy,transform); //instancio un nuevo enemigo
            float r = Random.Range(-4f, 4f); //rango de spawn
            newEnemy.transform.localPosition = new Vector3(newEnemy.transform.localPosition.x-10f, r); //posición del nuevo enemigo
            score.GetComponent<score>().scoreValor = score.GetComponent<score>().scoreValor + 10f; //cada vez que aparece un enemigo suma 10 a la puntuación
        }
        
    }

}
