﻿using TMPro;
using UnityEngine;

// script que controla la puntuación
public class score : MonoBehaviour
{
    public float scoreValor; //la puntuación
    public TMP_Text maxScoreTxt; //el texto donde se muestra la puntuación
    public TMP_Text actScoreTxt;
    public GameObject player; //referencia al jugador

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("sumaValor", 1f, 1f); 
    }

    // Update is called once per frame
    void Update()
    {
        maxScoreTxt.text = "Max Score: " + scoreValor.ToString(); //asigno una string con la puntuación actualizada al texto
        maxScoreTxt.text = "Score Actual: " + scoreValor.ToString();
    }

    // cada segundo ejecuto esta función que suma 10 puntos a la puntuación
    void sumaValor()
    {
        if (player!=null) //si el jugador sigue vivo suma 10 puntos a la puntuación
        {
            scoreValor = scoreValor + 10f;
        }
        
    }
}