﻿using UnityEngine;
using Unity.MLAgents;
using TMPro;

public class DodgeBallsAgent : Agent
{
    private Vector3 posini;
    private float scoreMax = 0;
    private int dodgedBalls = 0;
    public TMP_Text maxScoreTxt, actScoreTxt;

    public override void Initialize()
    {
        posini = this.transform.localPosition;
    }

    private void FixedUpdate()
    {
        RequestDecision();
        intro();
    }

    public override void OnEpisodeBegin()
    {
        this.transform.localPosition = posini;
        this.GetComponent<Rigidbody2D>().velocity = Vector3.zero;        
    }

    public override void OnActionReceived(float[] vectorAction)
    {
        int actionVectorGravity = (int)Mathf.Floor(vectorAction[0]);
        int actionVectorMove = (int)Mathf.Floor(vectorAction[1]);
        if (actionVectorGravity == 1)
        {
            GetComponent<Rigidbody2D>().gravityScale = -(GetComponent<Rigidbody2D>().gravityScale);
            this.transform.GetChild(0).localPosition = new Vector3(
                this.transform.GetChild(0).localPosition.x * -1f,
                this.transform.GetChild(0).localPosition.y,
                this.transform.GetChild(0).localPosition.z
                );
            this.transform.GetChild(0).GetComponent<ParticleSystem>().Stop();
        }
        if (actionVectorMove == 1 && transform.localPosition.x >= -10)
            GetComponent<Rigidbody2D>().velocity = new Vector2(-7, GetComponent<Rigidbody2D>().velocity.y);
    }

    public override void Heuristic(float[] actionsOut)
    {
        if (Input.GetKeyDown(KeyCode.Space)) //invierte gravedad del personaje, para el sistema de partículas y las cambia de posición
            actionsOut[0] = 1;
        else
            actionsOut[0] = 0;

        if (Input.GetKey(KeyCode.A) && transform.localPosition.x >= -8.5f) //te mueves a la izquierda
            actionsOut[1] = 1;
        else
            actionsOut[1] = 0;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Enemy")
        {
            Debug.Log("no enemy");
            AddReward(0.1f);
            dodgedBalls++;
            if (GetCumulativeReward()>scoreMax)
            {
                AddReward(0.1f);
                scoreMax = GetCumulativeReward();
                maxScoreTxt.text = "Max Score: " + scoreMax + "Dodged balls: "+dodgedBalls;
            }
            actScoreTxt.text = "Actual Score: " + GetCumulativeReward() + "Dodged balls: " + dodgedBalls;
            Debug.Log(GetCumulativeReward());
        }
        if (collision.transform.tag == "OffBounds")
            EndEpisode();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Enemy")
        {
            AddReward(-2f);
            maxScoreTxt.text = "Max Score: " + scoreMax + "Dodged balls: " + dodgedBalls;
            Debug.Log(GetCumulativeReward());
            EndEpisode();
        }
        if (collision.transform.tag == "Floor")
            this.transform.GetChild(0).GetComponent<ParticleSystem>().Play();
    }

    private void intro()
    {
        if (this.gameObject.transform.localPosition.x < -4f)
            GetComponent<Rigidbody2D>().velocity = new Vector2(7, GetComponent<Rigidbody2D>().velocity.y); //te mueves a la derecha
        else
            GetComponent<Rigidbody2D>().velocity = new Vector2(0f, GetComponent<Rigidbody2D>().velocity.y); //no te mueves
    }

}
