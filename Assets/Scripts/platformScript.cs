﻿using UnityEngine;

// script de la plataforma inicial
public class platformScript : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (this.gameObject.transform.localPosition.x < -15f) //si la plataforma pasa de -15 en el eje de las x
        {
            deletear();

        }
        transform.Translate(Vector3.left * 3f * Time.deltaTime); //mueve la plataforma hacia la izquierda
        
    }

    // destruye la plataforma
    private void deletear()
    {
        GameObject.Destroy(this.gameObject); 
    }

    
}
