﻿using UnityEngine;
using UnityEngine.SceneManagement;

// script con el GameOver y control de escenas
public class GameManager : MonoBehaviour
{
    public GameObject restartBtn; //referencia al botón de restart

    // Start is called before the first frame update
    void Start()
    {
        restartBtn.SetActive(false); //desactivo el botón de reinicio
    }

    // Game Over
    public void GameOver ()
    {
        Debug.Log("Game Over"); //imprimo por consola "Game Over"
        //restartBtn.SetActive(true); //activo el botón de reinicio
    }

    // reinicio de escena
    public void Restart ()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().name); //cargo la escena activa
    }

}
